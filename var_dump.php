/**
 * 输出变量
 *
 * @param void $varVal 变量值
 * @param bool $isExit 是否输出变量之后就结束程序（TRUE:是 FALSE:否）
 */
function dump($varVal, $isExit = FALSE){
    ob_start();
    var_dump($varVal);
    $varVal = ob_get_clean();
    $varVal = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $varVal);
    echo '<pre>'.$varVal.'</pre>';
    $isExit && exit();
}